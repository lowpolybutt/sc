package com.lp.fyardlib

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Image(
    val id: String,
    val name: String,
    val url: String,
    val width: Int,
    val height: Int,
    val type: String,
    val background: Boolean,
    val smallUrl: String,
    val bigUrl: String,
    val model: ImageModelGallery?
): Parcelable