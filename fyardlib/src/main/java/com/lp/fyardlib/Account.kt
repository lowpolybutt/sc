package com.lp.fyardlib

data class Account(
    val login: String,
    val password: String
)