package com.lp.sc.twitter

import com.google.gson.annotations.SerializedName

data class TwitterJwtResponse(
    @SerializedName("token_type") val tokenType: String,
    @SerializedName("access_token") val accessToken: String
) {
    override fun toString(): String {
        return "Token Type: $tokenType; Token: $accessToken"
    }
}