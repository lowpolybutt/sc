package com.lp.sc.twitter.models

data class Option(
    val position: Int,
    val text: String
)