package com.lp.sc.twitter.models

data class Size(
    val thumb: SizeDetails?,
    val large: SizeDetails?,
    val medium: SizeDetails?,
    val small: SizeDetails?
)