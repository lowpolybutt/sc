package com.lp.sc.twitter.models

data class SizeDetails(
    val h: Int,
    val w: Int,
    val resize: String
)