package com.lp.sc.twitter.models

data class Poll(
    val options: List<Option>
)