package com.lp.sc.twitter.models

import com.google.gson.annotations.SerializedName

data class Media(
    @SerializedName("display_url") val displayUrl: String,
    @SerializedName("expanded_url") val expandedUrl: String,
    val id: Long,
    val indices: List<Int>,
    @SerializedName("media_url") val mediaUrl: String,
    @SerializedName("media_url_https") val secureMediaUrl: String,
    val sizes: Size,
    @SerializedName("source_status_id") val srcStatusId: Long?,
    @SerializedName("source_status_string") val srcStatusString: Long?,
    val type: String,
    val url: String
)