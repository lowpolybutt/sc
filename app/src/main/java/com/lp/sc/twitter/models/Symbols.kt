package com.lp.sc.twitter.models

data class Symbols(
    val indices: List<Int>,
    val text: String
)