package com.lp.sc.twitter

import retrofit2.Call
import retrofit2.http.*

interface TwitterApi {
    @FormUrlEncoded
    @POST("oauth2/token")
    fun getBearerToken(@Field("grant_type") type: String): Call<TwitterJwtResponse>
}